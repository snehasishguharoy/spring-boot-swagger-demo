package com.demo.rest.employee.model;

import io.swagger.annotations.ApiModelProperty;

public class Employee {

	public Employee(Integer empId, String fname, String lname) {
		super();
		this.setEmpId(empId);
		this.fname = fname;
		this.lname = lname;
	}

	@ApiModelProperty(notes="id of the employee")
	private Integer empId;
	@ApiModelProperty(notes="first name of the employee")
	private String fname;
	@ApiModelProperty(notes="last name of the employee")
	private String lname;


	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

}
