package com.demo.rest.employee.cntlr;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.rest.employee.model.Employee;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/rest/emp")
@Api(value="Employee Resouce",description="Shows the employee information")
public class EmployeeResource {

	private static List<Employee> employees = new ArrayList<>();

	static {
		employees.add(new Employee(101, "Samik", "Roy"));
		employees.add(new Employee(201, "Tapas", "Roy"));
		employees.add(new Employee(301, "Arpan", "Roy"));
		employees.add(new Employee(401, "Snehasish", "Guha Roy"));

	}

	@GetMapping("/emps")
	public List<Employee> getAllEmps() {
		return employees;
	}

	@GetMapping("/{empId}")
	public Employee getEmpById(@PathVariable("empId") final int empId) {
		return employees.get(empId);
	}

}
